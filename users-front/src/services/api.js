import axios from 'axios';

const api = axios.create({
    baseURL : 'https://user-occupations.herokuapp.com'
})

export default api;
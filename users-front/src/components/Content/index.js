import React from 'react';
import Container from '@material-ui/core/Container';
import './styles.css'

const Content = (props) => (
    <Container className="content" maxWidth="lg">
        {props.children}
    </Container>
);

export default Content;

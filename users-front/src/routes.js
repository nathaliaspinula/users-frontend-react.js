import React from 'react';

import { Switch, Route } from 'react-router-dom';

import Main from './pages/main';
import NewUser from './pages/newUser';
import User from './pages/user';

const Routes = () =>
(
    <Switch>
        <Route exact path="/" component={Main}></Route>
        <Route exact path="/newUser" component={NewUser}></Route>
        <Route path="/users/:id" component={User}></Route>
    </Switch>
);

export default Routes;

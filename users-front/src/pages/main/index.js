import React, {Component} from 'react';
import Content from '../../components/Content'
import api from '../../services/api';
import MaterialTable from "material-table";
import InfoIcon from '@material-ui/icons/Info';
import {Link} from 'react-router-dom';
import './styles.css';


export default class Main extends Component{
    state = {
        users : [],
        usersInfo : [],
        page : 1
    }

    componentDidMount() {
        this.loadUsers();
    }

    loadUsers = async (page = 1) => {
        const response = await api.get('/users',{ headers: {"Access-Control-Allow-Origin": "*"}});
        const users = response.data;
        this.setState({users : users, page});
    }

    render() {
        const {users} = this.state;
        return (
            <div>
                <Content>
                <MaterialTable
                    data= {users}
                    title="Users"
                    columns={[
                        { title: 'CPF', field: 'cpf' },
                        { title: 'Name', field: 'name' },
                        { title: 'Surname', field: 'surname' },
                        { title: 'Email', field: 'email' },
                        { title: 'Cellphone', field: 'cellphone' },
                        { title: 'Birthday', field: 'birthday', type:'date'},
                        {
                          field: 'id',
                          title: 'See More',
                        render: rowData => <Link to={`/users/${rowData.id}`}><InfoIcon></InfoIcon></Link>
                        }
                      ]}
                    />
                </Content>

            </div>
        )
    }
}

import React, {Component} from 'react'
import Content from '../../components/Content'
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Typography  from '@material-ui/core/Typography';
import Card  from '@material-ui/core/Card';
import Button  from '@material-ui/core/Button';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import FormControl from '@material-ui/core/FormControl';
import api from '../../services/api';
import { cpfMask, cpfCheck, emailCheck } from '../../components/Mask'
import axios from '../../services/api'
import './styles.css'

export default class UserForm extends Component{
    state = {
        name: "",
        surname: "",
        cpf: "",
        email: "",
        cellphone: "",
        birthday: moment().format("YYYY-MM-DD"),
        occupations: [],
        selectedOccupations: []
    }

    componentDidMount()
    {
      this.loadOccupations();
    }

    loadOccupations = async () => {
      const response = await api.get('/occupations',{ headers: {"Access-Control-Allow-Origin": "*"}});
      const occupationsResult = response.data;
      this.setState({occupations : occupationsResult});
    }

    handleCpfChange = async (event) => {
      await this.setState({cpf: cpfMask(event.target.value)});
    }

    handleNameChange = async (event) => {
      await this.setState({name: event.target.value});
    }

    handleSurnameChange = async (event) => {
      await this.setState({surname: event.target.value});
    }

    handleEmailChange = async(event) => {
      await this.setState({email : event.target.value})
    }

    handleDateChange = async (event) => {
      const selectedDate = event.format("YYYY-MM-DD");
      await this.setState({birthday: selectedDate});
    }

    handleCellphoneChange = async (event) => {
      await this.setState({cellphone: event.target.value});
    }

    handleOccupationChange = async (event) => {
      await this.setState({ selectedOccupations: event.target.value});
    };

    saveOccupation = async (id) =>{
      await axios.post(`users/${id}/occupations`,
      {
        name: this.state.selectedOccupations
      }).then(response=>{
        return true;
      }).catch(error => {
        return false;
      })
    }

    save = async (e) => {
      const cpfState = this.state.cpf;
      const cpf = cpfState.replace(/[-.]/g, "");
      const name = this.state.name;
      const surname = this.state.surname;
      if( name &&
          surname &&
          cpfCheck(cpf) &&
          emailCheck(this.state.email))
      {
        await axios.post('/users', {
          name: this.state.name,
          surname: this.state.surname,
          cpf: this.state.cpf,
          cellphone: this.state.cellphone,
          birthday: this.state.birthday,
          email: this.state.email
        }).then(response => {
          const id = response.data.id;
          const success = this.saveOccupation(id);
          if(success){
            alert("sucesso");
          }
          else{
            alert("ocorreu um erro");
          }
        })
        .catch(error => {
            alert(error.response.data.error);
        });
      }
    }

    render(){
        return(
            <Content>
              <Card className="card-content">
                <Typography variant="h5">Register New User</Typography>
                <div className="form-control">
                  <FormControl onSubmit={this.handleSubmit}>
                    <TextField
                      id="cpf"
                      name="cpf"
                      required
                      label="Cpf"
                      className="form-input field"
                      value={this.state.cpf}
                      onChange={this.handleCpfChange}/>
                  </FormControl>
                  <FormControl>
                    <TextField
                      id="name"
                      name="name"
                      required
                      label="Name"
                      className="form-input field"
                      onChange={this.handleNameChange}/>
                  </FormControl>
                  <FormControl>
                    <TextField
                      id="surname"
                      name="surname"
                      required
                      label="Surname"
                      className="form-input field"
                      onChange={this.handleSurnameChange}/>
                  </FormControl>
                  <FormControl>
                    <TextField
                      id="email"
                      name="email"
                      required
                      label="Email"
                      className="form-input field"
                      onChange={this.handleEmailChange}/>
                  </FormControl>
                  <FormControl>
                    <TextField
                      id="cellphone"
                      name="cellphone"
                      required
                      label="Cellphone"
                      className="form-input field"
                      onChange={this.handleCellphoneChange}/>
                  </FormControl>
                  <FormControl>
                    <MuiPickersUtilsProvider
                      utils={MomentUtils}>
                      <KeyboardDatePicker
                      label="Birthday"
                      format="DD/MM/YYYY"
                      value={this.state.birthday}
                      onChange={this.handleDateChange}
                      className="form-input field"/>
                    </MuiPickersUtilsProvider>
                  </FormControl>
                  <FormControl>
                    <InputLabel>Occupation</InputLabel>
                    <Select
                      value={this.state.selectedOccupations}
                      input={<Input />}
                      onChange={this.handleOccupationChange}
                      >
                      {this.state.occupations.map(occupation => (
                        <MenuItem key={occupation.name} value={occupation.name}>
                          {occupation.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <Button className="save-button" variant="contained" color="primary" disableElevation onClick={this.save}>
                    Save
                  </Button>
                </div>
              </Card>
            </Content>
        );
    }
}

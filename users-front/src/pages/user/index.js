import React, {Component} from 'react';
import api from '../../services/api';
import moment from 'moment'
import './styles.css';

export default class User extends Component {
    state = {
        user: {}
    };

    async componentDidMount() {
        const {id} = this.props.match.params;
        const response = await api.get(`/users/${id}`);
        const result = {...response.data, birthday: moment(response.data.birthday).format("DD/MM/YYYY")}
        await this.setState({user: result});
    }

    render() {
        const {user} = this.state;
        return(
            <div className="user-info">
                <h1>{user.name} {user.surname}</h1>
                <p>{user.cpf}</p>
                <p>{user.email}</p>
                <p>{user.cellphone}</p>
                <p>{user.birthday}</p>
                <p>Occupation:</p>
                {user && user.occupations && user.occupations.map(occupation =>
                  <span key={occupation.name}>{occupation.name}</span>
                )}
            </div>
        );
    }
}

import React from 'react';
import Routes from './routes.js';
import Header from './components/Header';
import history from './services/history';
import { Router } from 'react-router-dom';

function App() {
  return (
    <div>
      <Router history={history}>
        <Header/>
        <Routes/>
      </Router>
      
    </div>
  );
}

export default App;
